<?php
namespace App\View\Composers;

use App\Models\Admin\Page;
use App\Models\Admin\Setting;
use App\Models\Admin\Technology;
use Illuminate\View\View;

/**
 *
 */
class InjectPages
{
    protected $page;
    protected $Setting;
    protected $technology;
    public function __construct(Page $page, Setting $setting, Technology $technology)
    {
        $this->page       = $page;
        $this->setting    = $setting;
        $this->technology = $technology;
    }

    public function compose(View $view)
    {
        $pages        = $this->page->where('hidden', false)->where('page_status', '1')->get()->toHierarchy();
        $settings     = $this->setting->get()->first();
        $technologies = $this->technology->where('status', 1)->orderBy('title', 'ASC')->get();
        $view->with(compact('pages', 'settings', 'technologies'));
    }

    public function output($pages)
    {
        $string = '';
        foreach ($pages as $i => $page) {

            $string .= '<li class="';

            // $string .= Request::is($page->slugWildCard) ? 'active' : '';

            $string .= count($page->children) ? ($page->isChild() ? 'dropdown-submenu' : 'dropdown') : '';
            $string .= '">';
            $string .= '<a href="';
            $string .= $page->slug;
            $string .= '" class="mdl-navigation__link mdl-typography--text-uppercase">';
            $string .= $page->page_title;

            if (count($page->children)) {

                $string .= '<span class="caret';
                $string .= $page->isChild() ? 'right' : '';
                $string .= '"></span>';
            }

            $string .= '</a>';

            if (count($page->children)) {

                $string .= '<ul class="dropdown-menu">';

                $string .= $this->output($page->children);

                $string .= '</ul>';
            }

            $string .= '</li>';

        }

        return $string;
    }

}
