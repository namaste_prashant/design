<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table    = 'sections';
    protected $fillable = ['title', 'status', 'description'];

    public function sectionList()
    {
        return $this->where('status', 1)->select('id', 'title')->orderBy('title', 'ASC')->get();
    }

    public function pages()
    {
        return $this->belongsToMany('App\Models\Admin\Page', 'page_section', 'section_id', 'page_id');
    }
}
