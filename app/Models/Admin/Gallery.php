<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table      = "galleries";
    protected $fillable   = ['title', 'description'];
    protected $morphClass = 'Flat';

    public function galleryList()
    {
        return $this->select('title', 'id')->get();
    }

    public function images()
    {
        return $this->hasMany('App\Models\Admin\Image', 'gallery_id', 'id');
    }

    public function photos()
    {
        return $this->morphMany('App\Models\Admin\Photo', 'imageable');
    }

    //Get the portfolio that belongs with the gallery.
    public function portfolios()
    {
        return $this->hasMany('App\Models\Admin\Portfolio');
    }

}
