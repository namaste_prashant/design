<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table    = 'posts';
    protected $fillable = ['author_id', 'title', 'slug', 'body', 'excerpt', 'published_at', 'status'];
    protected $data     = ['published_at']; // will make data a carbon instance.

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: null;
    }
}
