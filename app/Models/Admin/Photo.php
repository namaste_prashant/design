<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class Photo extends BaseModel
{
    protected $table    = "photos";
    protected $fillable = ['title', 'path', 'imageable_id', 'imageable_type', 'description', 'status', 'thumb'];

    /**
     * Get all of the owning imageable models.
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
