<?php

namespace App\Models\Admin;

use Baum\Node;

class Page extends Node
{
    protected $table    = 'pages';
    protected $fillable = ['page_title', 'slug', 'page_status', 'page_order', 'page_content', 'template', 'hidden', 'content_title', 'meta_key', 'meta_description', 'cover_image', 'banner_content', 'button_label', 'href_link', 'color', 'overlay'];

    public function pageList()
    {
        return $this->where('page_status', 1)->where('hidden', false)->select('page_title', 'id', 'depth')->orderBy('lft', 'asc')->get();
    }

    public function updateOrder($order, $orderPage)
    {
        $orderPage = $this->findOrFail($orderPage);

        if ($order == 'before') {
            $this->moveToLeftOf($orderPage);
        } elseif ($order == 'after') {
            $this->moveToRightOf($orderPage);
        } elseif ($order == 'childOf') {
            $this->makeChildOf($orderPage);
        }
    }

    public function setTemplateAttribute($value)
    {
        $this->attributes['template'] = $value ?: null;
    }

    public function sections()
    {
        return $this->belongsToMany('App\Models\Admin\Section', 'page_section', 'page_id', 'section_id')->withPivot('status', 'order');
    }

}
