<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    protected $table    = 'page_section';
    protected $fillable = ['page_id', 'section_id', 'order', 'status'];

    public function section()
    {
        return $this->belongsTo('App\Models\Admin\Section', 'section_id');
    }
}
