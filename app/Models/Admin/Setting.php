<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table    = 'settings';
    protected $fillable = ['logo', 'email', 'company_name', 'city1', 'city2', 'address1', 'address2', 'phone1', 'phone2', 'map1', 'map2'];
}
