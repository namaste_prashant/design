<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table    = 'portfolios';
    protected $fillable = ['title', 'slug', 'description', 'url', 'logo_image', 'category', 'technology', 'published_at', 'status', 'category_id', 'gallery_id'];
    protected $data     = ['published_at']; // will make data a carbon instance.

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: null;
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Admin\PortfolioCategory', 'category_portfolio', 'portfolio_id', 'category_id');
    }

    public function technologies()
    {
        return $this->belongsToMany('App\Models\Admin\Technology', 'portfolio_technology', 'portfolio_id', 'technology_id');
    }

    // portfolio has one gallery.
    public function gallery()
    {
        return $this->belongsTo('App\Models\Admin\Gallery');
        // return $this->hasOne('App\Models\Admin\Gallery', 'id', 'gallery_id');
    }

    public function updatePortfolio($input)
    {
        $portfolio      = $this->findOrFail($input['id']);
        $categories_ids = [];
        foreach ($input['categories'] as $category) {
            array_push($categories_ids, $category['id']);
        }
        $portfolio->categories()->sync($categories_ids);

        $technology_ids = [];
        foreach ($input['technologies'] as $technology) {
            array_push($technology_ids, $technology['id']);
        }
        $portfolio->technologies()->sync($technology_ids);

        return $portfolio->update($input);
    }

    public function createPortfolio($input)
    {
        $portfolio      = $this->create($input);
        $categories_ids = [];
        $technology_ids = [];
        //  create categories for portfolio
        foreach ($input['categories'] as $category) {
            array_push($categories_ids, $category['id']);
        }
        $portfolio->categories()->sync($categories_ids);

        // create technologies for portfolio.
        foreach ($input['technologies'] as $technology) {
            array_push($technology_ids, $technology['id']);
        }
        $portfolio->technologies()->sync($technology_ids);

        return $portfolio;
    }
}
