<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    protected $table    = 'technologies';
    protected $fillable = ['title', 'status', 'description', 'logo_image'];

    public function technologyList()
    {
        return $this->with('portfolios')->where('status', 1)->get();
    }

    public function portfolios()
    {
        return $this->belongsToMany('App\Models\Admin\Portfolio', 'portfolio_technology', 'technology_id', 'portfolio_id');
    }

}
