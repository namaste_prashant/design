<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategory extends Model
{
    protected $table    = 'portfolio_categories';
    protected $fillable = ['title', 'status', 'description', 'slug'];

    public function categoryList()
    {
        return $this->with(['portfolios' => function ($query) {
            $query->where('status', 1);
        }])->where('status', 1)->get();
    }

    public function portfolios()
    {
        return $this->belongsToMany('App\Models\Admin\Portfolio', 'category_portfolio', 'category_id', 'portfolio_id');
    }

}
