<?php
namespace App\Templates;

use App\Templates\AbstractTemplate;
use Illuminate\View\View;

class HomeTemplate extends AbstractTemplate
{
    protected $view = 'home';

    public function prepare(View $view, array $parameters)
    {
        $data = 'This is returned by the home template';
        $view->with('data', $data);
    }
}
