<?php
namespace App\Templates;

use App\Models\Admin\Post;
use App\Templates\AbstractTemplate;
use Illuminate\View\View;

class BlogTemplate extends AbstractTemplate
{
    protected $view = 'blog';

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function prepare(View $view, array $parameters)
    {

        $posts = $this->post->orderBy('created_at', 'desc')->paginate(2);

        $view->with('posts', $posts);
    }
}
