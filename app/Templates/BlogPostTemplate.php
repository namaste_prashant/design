<?php
namespace App\Templates;

use App\Models\Admin\Post;
use App\Templates\AbstractTemplate;
use Illuminate\View\View;

class BlogPostTemplate extends AbstractTemplate
{
    protected $view = 'blog.post';

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function prepare(View $view, array $parameters)
    {
        $post = $this->post->where('id', $parameters['id'])->where('slug', $parameters['slug'])->first();

        $view->with('post', $post);
    }
}
