<?php

namespace App\Providers;

use App\View\Composers\InjectPages;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['view']->composer('*', InjectPages::class);

        // Polymorphic map for photos modal.
        Relation::morphMap([
            'Gallery' => \App\Models\Admin\Gallery::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
