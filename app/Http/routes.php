<?php

Route::controller('auth/password', 'Auth\PasswordController', [
    'getEmail' => 'auth.password.email',
    'getReset' => 'auth.password.reset',
]);

Route::controller('auth', 'Auth\AuthController', [
    'getLogin' => 'auth.login',
]);

Route::get('admin', ['as' => 'admin.home', 'uses' => 'Admin\HomeController@index']);

Route::post('/send_message', 'ContactusController@sendMessage');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

    resource('pages', 'PagesController');
    get('page_list', 'PagesController@getList');
    get('get_page_templates', 'PagesController@getPageTemplates');

    resource('posts', 'PostsController');

    resource('gallery', 'GalleriesController');
    get('gallery_list', 'GalleriesController@getList');

    resource('portfolio', 'PortfoliosController');

    resource('photo', 'PhotosController');

    resource('section', 'SectionsController');
    get('section_list', 'SectionsController@getList');

    resource('portfolio_category', 'PortfolioCategoriesController');
    get('portfolio_category_list', 'PortfolioCategoriesController@getList');

    resource('page_section', 'PageSectionsController');
    post('update_page_sections', 'PageSectionsController@updatePageSections');

    resource('setting', 'SettingsController');

    resource('technology', 'TechnologiesController');
    get('technology_list', 'TechnologiesController@getList');
});

Route::get('portfolio', 'PortfoliosController@index');
Route::get('portfolio_category_list', 'PortfolioCategoriesController@getList');
Route::get('gallery/{title}', 'GalleriesController@show');

Route::get('page', function () {
    return view('page');
});

Route::get('content', function () {
    return View('pages');
});

Route::get('create', function () {
    return View('create');
});
