<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\Setting;
use Illuminate\Http\Request;

class SettingsController extends AdminController
{
    protected $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result['data']    = $this->setting->get()->first();
        $result['success'] = true;
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->input('data');

        // If file there is file upload it.
        if ($request->hasFile('file')) {
            $file          = $request->file('file');
            $input['logo'] = $this->uploadImage($this->logoPath, $file);
        }

        // If there's id in data then update the model.
        // Else create new one.
        if (isset($input['id'])) {
            $setting        = $this->setting->findOrFail($input['id']);
            $result['data'] = $setting->update($input);
        } else {
            $result['data'] = $this->setting->create($input);
        }

        $result['success'] = true;
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting           = $this->setting->findOrFail($id);
        $result['data']    = $setting->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }
}
