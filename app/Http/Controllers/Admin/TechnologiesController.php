<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\Technology;
use Illuminate\Http\Request;

class TechnologiesController extends AdminController
{
    protected $technology;
    protected $logo_image_path = "uploads/images";
    public function __construct(Technology $technology)
    {
        $this->technology = $technology;
    }

    public function store(Request $request)
    {
        $input = $request->input('data');

        if ($request->hasFile('file')) {
            $file                = $request->file('file');
            $input['logo_image'] = $this->uploadImage($this->logo_image_path, $file);
        }

        if (isset($input['id'])) {
            $result['data'] = $this->technology->find($input['id'])->update($input);
        } else {
            $this->technology->create($input);
        }

        $result['success'] = true;
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->technology;

        if ($request->has('currentPage')) {
            $this->current_page = $request->input('currentPage');
        }

        $skip            = ($this->current_page - 1) * $this->per_page;
        $result['total'] = $query->get()->count();
        $result['data']  = $query->skip($skip)->take($this->per_page)->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $result['data']    = $this->technology->create($request->input('data'));
    //     $result['success'] = true;
    //     return $result;
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $technology        = $this->technology->findOrFail($id);
        $result['data']    = $technology->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->technology->destroy($id) ? $result['success'] = true : $result['success'] = false;
        return $result;
    }

    /**
     * Return list for select box.
     * @return object
     */
    public function getList()
    {
        $result['data'] = $this->technology->technologyList();
        return $result;
    }

}
