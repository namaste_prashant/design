<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\Photo;
use Illuminate\Http\Request;

class PhotosController extends AdminController
{
    protected $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->photo;

        if ($request->has('imageable_id')) {
            $query = $query->where('imageable_id', $request->input('imageable_id'));
        }

        if ($request->has('currentPage')) {
            $this->current_page = $request->input('currentPage');
        }

        $skip            = ($this->current_page - 1) * $this->per_page;
        $result['total'] = $query->get()->count();
        $result['data']  = $query->skip($skip)->take($this->per_page)->get();

        //Add photos meta data to the collection.
        $result['data']->each(function ($item, $key) {
            if (file_exists($item->path)) {
                $item->setAttribute('file_size', filesize($item->path));
                $item->setAttribute('dimension', $this->getDimension($item->path));
            }
        });
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input('data');

        if ($request->hasFile('file')) {
            $file           = $request->file('file');
            $input['path']  = $this->uploadGalleryImage($file);
            $input['thumb'] = $this->createThumb($input['path']);
        }

        $result['data']    = $this->photo->create($input);
        $result['success'] = true;
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo             = $this->photo->findOrFail($id);
        $result['data']    = $photo->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->photo->destroy($id) ? $result['success'] = true : $result['success'] = false;
        return $result;
    }

}
