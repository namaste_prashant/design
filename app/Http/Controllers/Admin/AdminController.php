<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use Image;

abstract class AdminController extends BaseController
{

    /**
     * Variable to store items per page for pagination.
     * @var integer
     */
    protected $per_page = 10;

    /**
     * Default page no. for pagination.
     * @var integer
     */
    protected $current_page = 1;

    protected $logoPath = "uploads/images/";

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function uploadImage($path, $file)
    {

        // If path is not found make one.
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        // Create unique file name.
        $filename = $path . '/' . round(microtime(true)) . $file->getClientOriginalName();

        // Upload the file with new name.
        $file->move($path, $filename);

        // Return the new filename.
        return $filename;
    }

    protected function uploadCoverImage($file)
    {
        $destinationPath = 'uploads/cover_images';

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $filename = $destinationPath . '/' . round(microtime(true)) . $file->getClientOriginalName();
        $file->move($destinationPath, $filename);
        return $filename;
    }

    protected function uploadGalleryImage($file)
    {
        $destinationPath = 'uploads/gallery_images';

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $extension = $file->getClientOriginalExtension();
        $filename  = round(microtime(true)) . $file->getClientOriginalName();
        $fullpath  = $destinationPath . '/' . $filename;
        $file->move($destinationPath, $filename);

        return $fullpath;
    }

    protected function createThumb($source, $destinationPath = 'uploads/thumb')
    {

        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        // explode and source and get last element as file name.
        $filename = end((explode('/', $source)));

        // create instance
        $img = Image::make($source);

        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        $img->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $thumbpath = $destinationPath . '/' . $filename;

        // finally we save the image as a new file
        $img->save($thumbpath);
        return $thumbpath;
    }
}
