<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\PageSection;
use Illuminate\Http\Request;

class PageSectionsController extends AdminController
{
    protected $pageSection;

    public function __construct(PageSection $pageSection)
    {
        $this->pageSection = $pageSection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->pageSection->with('section')->orderBy('order');

        if ($request->has('page_id')) {
            $query->where('page_id', $request->input('page_id'));
        }

        if ($request->has('currentPage')) {
            $this->current_page = $request->input('currentPage');
        }

        $skip            = ($this->current_page - 1) * $this->per_page;
        $result['total'] = $query->get()->count();
        $result['data']  = $query->skip($skip)->take($this->per_page)->get();

        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input       = $request->input('data');
        $pageSection = $this->pageSection->where('page_id', $input['page_id'])->where('section_id', $input['section_id'])->get();

        // Create page section if not aleready created.
        if (count($pageSection) == 0) {
            $result['data']    = $this->pageSection->create($input);
            $result['success'] = true;
        } else {
            $result['success'] = false;
            $result['msg']     = 'Item Already added';
        }

        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pageSection       = $this->pageSection->findOrFail($id);
        $result['data']    = $pageSection->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->pageSection->destroy($id) ? $result['success'] = true : $result['success'] = false;
        return $result;
    }

    /**
     * Return list for select box.
     * @return object
     */
    public function getList()
    {
        $result['data'] = $this->pageSection->pageSectionList();
        return $result;
    }

    /**
     * Update page sections
     * @param  Request $request
     * @return [array]
     */
    public function updatePageSections(Request $request)
    {
        foreach ($request->all() as $data) {
            $this->pageSection->findOrFail($data['id'])->update($data);
        }

        $result['success'] = true;
        return $result;
    }
}
