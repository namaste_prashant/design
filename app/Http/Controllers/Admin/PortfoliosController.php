<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\Portfolio;
use Illuminate\Http\Request;

class PortfoliosController extends AdminController
{
    protected $portfolio;
    protected $logo_image_path = "uploads/images";

    public function __construct(Portfolio $portfolio)
    {
        $this->portfolio = $portfolio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result['data'] = $this->portfolio->with(['categories', 'technologies'])->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input('data');

        if ($request->hasFile('file')) {
            $file                = $request->file('file');
            $input['logo_image'] = $this->uploadImage($this->logo_image_path, $file);
        }

        if (isset($input['id'])) {
            $result['data'] = $this->portfolio->updatePortfolio($input);
        } else {
            $this->portfolio->createPortfolio($input);
        }

        $result['success'] = true;
        return $result;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $portfolio         = $this->portfolio->findOrFail($id);
        $result['data']    = $portfolio->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->portfolio->destroy($id) ? $result['success'] = true : $result['success'] = false;
        return $result;
    }

    /**
     * Return list for select box.
     * @return object
     */
    public function getList()
    {
        $result['data'] = $this->portfolio->portfolioList();
        return $result;
    }

}
