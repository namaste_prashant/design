<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Admin\Page;
use Baum\MoveNotPossibleException;
use Illuminate\Http\Request;

class PagesController extends AdminController
{
    protected $page;
    protected $cover_image_path = "uploads/cover_images";

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->page;
        if ($request->has('currentPage')) {
            $this->current_page = $request->input('currentPage');
        }

        $skip            = ($this->current_page - 1) * $this->per_page;
        $result['total'] = $query->get()->count();
        $result['data']  = $query->skip($skip)->take($this->per_page)->orderBy('lft', 'asc')->get();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input('data');

        //  Update the cover image.
        if ($request->hasFile('file')) {
            $file                 = $request->file('file');
            $input['cover_image'] = $this->uploadImage($this->cover_image_path, $file);
        }

        if (isset($input['id'])) {
            $result['data'] = $this->page->findOrFail($input['id'])->update($input);
        } else {
            $result['data'] = $this->page->create($input);
            $this->updatePageOrder($result['data'], $request);
        }

        $result['success'] = true;
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result['data']    = $this->page->findOrFail($id);
        $result['success'] = true;
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = $this->page->findOrFail($id);

        if ($response = $this->updatePageOrder($page, $request)) {
            return $response;
        }

        $result['data']    = $page->update($request->input('data'));
        $result['success'] = true;
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->page->destroy($id) ? $result['success'] = true : $result['success'] = false;
        return $result;
    }

    /**
     * Return list for select box.
     * @return object
     */
    public function getList()
    {
        $result['data'] = $this->page->pageList();
        return $result;
    }

    protected function updatePageOrder(Page $page, Request $request)
    {
        if (array_key_exists('order', $request->input('data')) && array_key_exists('orderPage', $request->input('data'))) {

            try
            {
                $page->updateOrder($request->input('data')['order'], $request->input('data')['orderPage']);

            } catch (MoveNotPossibleException $e) {

                return "Cannot make a page a child ";
            }
        }
    }
    protected function getPageTemplates()
    {
        $templates      = config('cms.templates');
        $result['data'] = array_combine(array_keys($templates), array_keys($templates));
        return $result;
    }

}
