<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Make Dimension of the image.
     * @param  [string] $url [full path of the image]
     * @return [string]      [dimension of the image]
     */
    protected function getDimension($url)
    {

        if (isset($url)) {
            list($width, $height) = getimagesize($url);
            return $width . 'X' . $height;
        }
    }

}
