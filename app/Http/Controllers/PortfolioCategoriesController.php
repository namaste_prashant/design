<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\PortfolioCategory;

class PortfolioCategoriesController extends Controller
{
    protected $portfolioCategory;

    public function __construct(PortfolioCategory $portfolioCategory)
    {
        $this->portfolioCategory = $portfolioCategory;
    }

    /**
     * Return list for select box.
     * @return object
     */
    public function getList()
    {
        $result['data'] = $this->portfolioCategory->categoryList();
        return $result;
    }

}
