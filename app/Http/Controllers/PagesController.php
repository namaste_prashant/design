<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Page;
use App\Models\Admin\Portfolio;
use App\Models\Admin\PortfolioCategory;

class PagesController extends Controller
{

    protected $portfolioCategory;
    protected $portfolio;
    public function __construct(PortfolioCategory $portfolioCategory, Portfolio $portfolio)
    {
        $this->portfolioCategory = $portfolioCategory;
        $this->portfolio         = $portfolio;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page, array $parameters)
    {
        /**
         * This logic specific the this application only.
         * If slug of page is same as portfolio category.
         * Retrive all portfolios related to that category.
         */
        $protfolios          = null;
        $portfolioCategories = $this->portfolioCategory->all();
        foreach ($portfolioCategories as $portfolioCategory) {
            if ($portfolioCategory->slug == $page->slug) {
                $protfolios = $portfolioCategory->portfolios()->with('technologies')->get();
            }
        }

        /**
         * This logic specific the this application only.
         * If slug of page is same as portfolio.
         * Retrive all portfolios related to that portfolio.
         */
        $portfolio      = null;
        $portfolio_list = $this->portfolio->all();
        foreach ($portfolio_list as $p) {
            if ($p->slug == $page->slug) {
                $portfolio = $p->load(['technologies', 'gallery' => function ($query) {
                    $query->with(['photos' => function ($query) {
                        $query->where('status', 1);
                    }]);
                }]);
            }
        }

        $this->prepareTemplate($page, $parameters);
        return view('page', compact('page', 'protfolios', 'portfolio'));
    }

    protected function prepareTemplate(Page $page, array $parameters)
    {
        $templates = config('cms.templates');

        // If template is not defined in page or
        // not found in cms.php file killl the execution.
        if (!$page->template || !isset($templates[$page->template])) {
            return;
        }

        // Instantiate the template by providing it in app.
        // it will handle all dependancey withing it.
        $template = app($templates[$page->template]);

        // Grab the full name of the templat view.
        $view = sprintf('templates.%s', $template->getView());

        if (!view()->exists($view)) {
            return;
        }

        // Pass view instance and parameters to the prepare method of the template.
        $template->prepare($view = view($view), $parameters);

        // Set the view to the Page model.
        $page->view = $view;
    }

}
