<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Portfolio;

class PortfoliosController extends Controller
{
    protected $portfolio;

    public function __construct(Portfolio $portfolio)
    {
        $this->portfolio = $portfolio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result['data'] = $this->portfolio->where('status', 1)->with(['categories', 'technologies'])->get();
        return $result;
    }

}
