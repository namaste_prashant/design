<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Gallery;

class GalleriesController extends Controller
{
    protected $gallery;

    public function __construct(Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * Return photos of the gallery.
     * @param  [type] $title [description]
     * @return [type]        [description]
     */
    public function show($title)
    {
        $result['data'] = $this->gallery->where('title', $title)->first()->photos;
        return $result;
    }
}
