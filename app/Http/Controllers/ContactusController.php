<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class ContactusController extends Controller
{

    public function sendMessage(Request $request)
    {
        $data = $request->all();

        Mail::send('emails.contactus', compact('data'), function ($message) {
            $message->to(config('mail.from.address'), config('mail.from.name'))->subject('IdeasOffshore Feedback.');
        });
        $result['sucess'] = true;
        return $result;
    }
}
