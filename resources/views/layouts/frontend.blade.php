<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Ideas Offshore Digital Agency.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')&mdash; Ideas Offshore | Digital Agency </title>

        <link rel="stylesheet" href="/bower_components/angular-material/angular-material.css">
        <meta name="google-site-verification" content="w_ptPLP4ULmj9QhirKbtfXgfxspHiXvpqiAWvh1YbXM" />
        <script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>

        {{--Angular-material library--}}
        <script type="text/javascript" src="/bower_components/angular/angular.js"></script>
        <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.js"></script>
        <script type="text/javascript" src="/bower_components/angular-aria/angular-aria.js"></script>
        <script type="text/javascript" src="/bower_components/angular-material/angular-material.js"></script>
        <script type="text/javascript" src="/bower_components/angular-resource/angular-resource.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::asset('css/material.min.css') }}">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
        <link rel="stylesheet" href="{{ URL::asset('css/common-frontend.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/home.css')}}"
    </head>


    <body ng-app="app" ng-controller="BaseController">

        @yield('content')

    <script type="text/javascript" src="{{ URL::asset('js/app.js')}}"></script>

    <script type="text/javascript" src="/frontend/services/rjServices.js"></script>
    <script type="text/javascript" src="frontend/directives/rjDirective.js"></script>
    <script type="text/javascript" src="frontend/directives/slide/slide.js"></script>
    <script type="text/javascript" src="frontend/directives/isotobes/isotobes.js"></script>

    <script type="text/javascript" src="frontend/app/app.js"></script>
    <script type="text/javascript" src="frontend/app/portfolio.js"></script>
    <script type="text/javascript" src="frontend/app/contactus.js"></script>
    <script type="text/javascript" src="frontend/app/image-gallery/brand.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58359489-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-53JMZV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-53JMZV');</script>
<!-- End Google Tag Manager -->
    </body>

</html>
