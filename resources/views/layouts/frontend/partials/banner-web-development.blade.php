<section class="banner-web-development">
	<div class="bg-creamy">
		<div class="container center section-banner">
            <h1 class="text-bd">Web Development</h1>
			<p>Ideasoffshore provides you range of services with stepwise software design process from wireframing, designing, coding, deployment, and maintenance. Our team at Ideaoffshore with strong background in different areas are committed to providing you the first class service. We value your investment in technology and therefore we are dedicated to deliver you the best.  </p>
        	<br>
        	<a href="#contact-us" class="md-button md-raised md-custom-center">Start a Project</a>
        </div>
	</div>
</section>