<section class="io-location" style="background-image:url('images/banner/8.jpg');height:500px;background-size:cover;background-position-y:15%">
    <div class="banner-image"></div>
    <div class="container">
        <div class="content-banner">
            <div layout="row">
                <div flex-gt-sm="100" flex="50">
                    <h1 class="text-bd">GET STARTED <br>ON YOUR <br> PROJECT</h1>
                </div>
                <div flex-gt-sm="100" flex="50">
                            <address class="pa-t-20">
                                <city class="text-h1">Sydney</city>
                                <city-details class="text-h3">6/1 George St, Marrickville, 2204, New South Wales</city-details>
                                <phone class="text-h3">+61-415947543</phone>
                                <gmap>
                                    <a href="http://maps.google.com?q=6/1 George St, Marrickville, 2204,  New South Wales" target="_blank">
                                        <i class="material-icons">place</i> <span class="icon-text">Map</span>
                                    </a>
                                </gmap>
                            </address>
                            <address class="pa-t-20">
                                <city class="text-h1">Kathmandu</city>
                                <city-details class="text-h3">Katyani Marg, Old Baneshwor, Nepal</city-details>
                                <phone class="text-h3">+977-9841208187</phone>
                                <gmap>
                                    <a href="http://maps.google.com?q=Ideas Offshore" target="_blank">
                                        <i class="material-icons">place</i> <span class="icon-text">Map</span>
                                    </a>
                                </gmap>
                            </address>
                </div>
            </div>
        </div>
    </div>
</section>