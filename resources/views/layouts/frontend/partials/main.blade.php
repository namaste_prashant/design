<section class="io-home-banner">
    <div class="landing-hero" style="background-image:url('{{$page->cover_image }}')">
        @if(! empty($page->banner_content))
        @if( $page->overlay == '1')
        <div class="bg-overlay"></div>
        @endif
        <div class="container">
            <div layout="column">
                <div flex><h1 class="text-center text-bd color-default {{$page->color}}">{{ $page->banner_content}}</h1></div>
                @if(! empty($page->button_label))
                <div flex><a href="{{ $page->href_link }}" class="md-button md-raised md-custom-center">{{ $page->button_label }}</a></div>
                @endif
            </div>
        </div>
        @endif
    </div>
     @if(! empty($page->content_title))
    <div class="bg-light-green bg-content">
        <div class="container center section-lg-banner">
            <h1 class="text-bd">{{ $page->content_title}}</h1>
			<p>{{ strip_tags($page->page_content) }}</p>	
		 </div>
    </div>
    @endif
    <div class="floating--button">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent">
        <i class="material-icons">expand_more</i>
        </button>
    </div>
</section>