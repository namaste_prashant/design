<section class="io-solution">
    <div class="bg-yellow">
        <ul class="">
            <li class="">
                <a href="/web-design-and-development">
                <div class="website-development section-solution section-side">
                    <h2>Web Site Design & Development</h2>
                </div>
                </a>
            </li>
            <li>
                <a href="/mobile-application">
                <div class="mobile-application section-solution section-mid">
                    <h2>Mobile Application Development</h2>
                </div>
                </a>
            </li>
            <li>
                <a href="/e-commerce">
                <div class="e-commerce section-solution section-side">
                    <h2>E-commerce</h2>
                </div>
                </a>
            </li>
            <li>
                <a href="/web-application">
                <div class="web-application section-solution section-side">
                    <h2>Web Application</h2>
                </div>
                </a>
            </li>
            <li>
                <a href="/search-engine-optimization">
                <div class="seo section-solution section-mid">
                    <h2>SEO</h2>
                </div>				
                </a>
            </li>
            <li>
                <a href="javascript:void(null)">
                <div class="social-application section-solution section-side">
                    <h2>Social Media Application Development & Integration</h2>
                </div>
                </a>
            </li>
        </ul>
    </div>
</section>