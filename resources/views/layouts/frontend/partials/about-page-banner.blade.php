<section class="io-about-banner">
    <div class="image-about">
        <div style="background-image:url('images/banner/6.jpg');height:420px;background-size:cover;background-position-y:45%"></div>
    </div>
    <div class="bg-light-green">
        <div class="container center section-30-banner">
            <h1>About Us</h1>
            <p>Started with a team of enthusiastic and technical expertise, Ideas offshore was established in early 2014 with the aim to closely work with its clients to provide top quality web services depending on their technical needs. Our mission is to deliver job satisfaction with our innovative and efficient services to our clients. Every team member at ideas offshore is dedicated to provide top quality service to go beyond the client’s expectation.</p>
        </div>
    </div>
    <div class="floating--button">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent">
        <i class="material-icons">expand_more</i>
        </button>
    </div>
    <div class="bg-white">
        <div class="container center section-lg-banner">
            <div class="quotation">
                <h3>Think of the most amazing way to do it, fastest way or cheapest way is not always the best solution.</h3>
            </div>
        </div>
    </div>
</section>