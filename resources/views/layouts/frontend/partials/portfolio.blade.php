
<section class="io-portfolio" style="background: #eee; height: 400px">
<div ng-controller="PortfolioController" class="sample" layout="column" ng-cloak>
  <md-content class="md-padding bg-gray">

    <md-tabs  md-border-bottom md-selected="selectedIndex" class="md-tabs-height bg-gray" ng-show="dataLoaded">
        <md-tab label="All">
          <md-content class="md-padding bg-gray" layout="row" layout-wrap layout-align="center start" layout-xs="column" >
            <div layout="column" ng-repeat="portfolio in portfolios" class="pull-left">
              <md-card>
                <img ng-src="/[[ portfolio.logo_image ]]" class="md-card-image fx-height" alt="Washed Out">
                <md-card-title>
                  <md-card-title-text>
                    <a href="/[[ portfolio.slug]]" class="text-default"> <span class="md-headline">[[ portfolio.title ]]</span></a>
                  </md-card-title-text>
                </md-card-title>
              </md-card>
            </div>
          </md-content>
        </md-tab>

        <md-tab ng-repeat="category in categories"  label="[[category.title]]">
            <md-content class="md-padding bg-gray" layout="row" layout-wrap layout-align="center start" layout-xs="column" >
                <div  layout="column" ng-repeat="portfolio in category.portfolios" class="pull-left">
                  <md-card>
                    <img ng-src="/[[ portfolio.logo_image ]]" class="md-card-image fx-height" alt="Washed Out">
                    <md-card-title>
                      <md-card-title-text>
                         <a href="/[[ portfolio.slug]]" class="text-default"><span class="md-headline">[[ portfolio.title ]]</span></a>
                      </md-card-title-text>
                    </md-card-title>
                  </md-card>
                </div>
            </md-content>
        </md-tab>
    </md-tabs>

  </md-content>

</div>

</section>
