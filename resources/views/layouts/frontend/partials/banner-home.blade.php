<section class="io-home-banner">
    <div class="landing-hero" style="background-image:url('images/banner/10.jpg')">
        <div class="container">
            <div layout="column">
                <div flex><h1 class="text-center text-bd color-gray">Full Service Digital Agency</h1></div>
                @if(!empty($page->href_link) && !empty($page->button_label))
                <div flex>
                    <a href="{{ $page->href_link }}" class="md-button md-raised md-custom-center">{{ $page->button_label }}</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="bg-light-green">
        <div class="container center section-30-banner">
            <h1 class="text-bd">Build your ideasOffshore</h1>
			<p>There is a tremendous amount of craftsmanship between great idea and great product. If your desire is to transform your innovative ideas for web or mobile application into reality, then we provide you with the smartest and the most cost effective solutions.</p>
        </div>
    </div>
    <div class="floating--button">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent">
        <i class="material-icons">expand_more</i>
        </button>
    </div>
</section>
