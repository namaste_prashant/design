<section class="io-technology-used">
	  <div class="bg-creamy bg-content">
	    <div class="container center section-lg-banner">
	    <h1 class="text-bd">Technology Used</h1>
	    <ul class="mid-center">
		@foreach($portfolio->technologies as $technology)
				<li>
					<img src="{{$technology->logo_image}}" />
					<md-tooltip md-direction="bottom"><p>{{$technology->title}}</p></md-tooltip>
				</li>
		@endforeach
		</div>
	</div>
	</div>
</section>
