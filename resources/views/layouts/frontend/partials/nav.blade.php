<section class="io-nav">
    <div class="navbar navbar-fixed">
        <!-- For Responsive Menu and Logo -->
        <div class="navbar-header">
            <button class="icon-fade navbar-toggle" id="sm-menu">
                <i class="material-icons">&#xE5D2;</i>
            </button>
            <a href="/" class="navbar-brand">
                <img alt="ideas offshore" height="50" class="ideasoffshore-big-logo" src="{{ isset($settings->logo)?$settings->logo:'' }}" width="140">
                <img alt="ideas Offshore small" height="35" class="ideasoffshore-mobile-logo" src="{{ isset($settings->logo)?$settings->logo:'' }}" width="92" >
            </a>
       </div>
        <!-- Main Menu Come here -->
        <ol class="nav navbar-nav navbar-md">
            @include('partials/navigation')
        </ol>
    </div>
    <div class="basement-menu" id="res-menu">
        <div class="basement-nav">
            <div class="basement-header">
                <img alt="ideas offshore" height="50" class="android-logo-image" src="{{ isset($settings->logo)?$settings->logo:'' }}" width="140">
            </div>
            <ul class="nav nav-pages navbar-sm">
                @include('partials/responsive')
            </ul>
        </div>
    </div>

</section>
