<section class="io-footer">
    <div class="bg-black">
        <div class="section-md-banner section-sm">
        	<div class="container">
	            <div layout="row" layout-sm="column" layout-align="space-around start" layout-align-sm="center start">
	                <div>
	                    <h4 class="color-green ma-b-0">
	                        Ideas Offshore
	                    </h4>
	                    <ul>
                            @foreach ($pages as $page)
                            <li>
                                <a href="/{{ $page->slug }}">
                                    {{ $page->page_title }}
                                </a>
                            </li>
                            @endforeach
	                    </ul>
	                </div>
                    <div>
                        <h4 class="color-green ma-b-0">
                            Products
                        </h4>
                        <ul>
                            @foreach ($pages as $page)
                                @if(count($page->children) && $page->page_title == 'Products')
                                    @foreach ($page->children as $child)
                                        <li>
                                            <a href="/{{ $child->slug }}">
                                                {{ $child->page_title }}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div>
                        <h4 class="color-green ma-b-0">
                            Solution
                        </h4>
                        <ul>
                            @foreach ($pages as $page)
                                @if(count($page->children) && $page->page_title == 'Solution')
                                    @foreach ($page->children as $child)
                                        <li>
                                            <a href="/{{ $child->slug }}">
                                                {{ $child->page_title }}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            @endforeach
                        </ul>
                    </div>
	            </div>
	   		</div>
	   	</div>
	</div>
	<div class="bg-shaded-black">
		<div class="section-footer section-sm">
        	<div class="container">
	            <div layout="row" layout-sm="column" layout-align="space-around end" layout-align-sm="center start">
                    <div>
                    	<h4 class="color-green ma-b-0">
	                        Location
	                    </h4>
                        <address>
                            <city class="text-h2 color-white">
                                {{ $settings->city1 }}
                            </city>
                            <city-details class="text-h3">
                                {{ $settings->address1 }}
                            </city-details>
                            <phone class="text-h3">
                                {{ $settings->phone1 }}
                            </phone>
                            <gmap>
                                <a href="{{ $settings->map1 }}" target="_blank">
                                    <i class="material-icons">
                                        place
                                    </i>
                                    <span class="icon-text">
                                        Map
                                    </span>
                                </a>
                            </gmap>
                        </address>
                    </div>
                    <div>
                        <address>
                            <city class="text-h2 color-white">
                                {{ $settings->city2 }}
                            </city>
                            <city-details class="text-h3">
                                {{ $settings->address2 }}
                            </city-details>
                            <phone class="text-h3">
                                {{ $settings->phone2 }}
                            </phone>
                            <gmap>
                                <a href="{{ $settings->map2 }}" target="_blank">
                                    <i class="material-icons">
                                        place
                                    </i>
                                    <span class="icon-text">
                                        Map
                                    </span>
                                </a>
                            </gmap>
                        </address>
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="bg-black">
		<div class="container">
	        <div layout="row" layout-align="center center">
	            <div>
	                <p class="color-white ma-t-8">
	                    &copy; 2016 | Ideas Offshore. All Rights Reserved
	                </p>
	            </div>
			</div>
		</div>
	</div>
</section>
