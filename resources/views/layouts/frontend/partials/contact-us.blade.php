<section class="io-contact-us" ng-controller="ContactusController" id="contact-us">

	<div class="bg-creamy">

		<div class="container section-md-banner">
		<h1>Start A Project</h1>

		<form name="Form">
			<div layout-gt-sm="row">
	          <md-input-container class="md-block" flex-gt-sm="">
	            <label>First name</label>
	            <input ng-model="dataModel.firstName" name="first_name" required>
		        <div ng-messages="Form.first_name.$error">
		            <div ng-message="required">This is required.</div>
		        </div>
	          </md-input-container>

	          <md-input-container class="md-block" flex-gt-sm="">
	            <label>Last Name</label>
	            <input ng-model="dataModel.lastName">
	          </md-input-container>
        	</div>
        	<div layout-gt-sm="row">
	          <md-input-container class="md-block" flex-gt-sm="">
	            <label>Email</label>
	            <input ng-model="dataModel.email" required name="email" type="email">
		        <div ng-messages="Form.email.$error">
		        	<div ng-message="email">Valid email is required.</div>
		            {{-- <div ng-message="required">This is required.</div> --}}
		        </div>
	          </md-input-container>

	          <md-input-container class="md-block" flex-gt-sm="">
	            <label>Phone</label>
	            <input ng-model="dataModel.phone">
	          </md-input-container>
        	</div>
        	<div layout-gt-sm="row">
	          <md-input-container class="md-block" flex-gt-sm="">
	            <label>Company Name</label>
	            <input ng-model="dataModel.company">
	          </md-input-container>
        	</div>
        	<md-input-container class="md-block">
          <label>Details</label>
          <textarea ng-model="dataModel.details" columns="1" md-maxlength="150" rows="5" name="details" required></textarea>
		  <div ng-messages="Form.details.$error">
			   <div ng-message="required">This is required.</div>
		  </div>
        </md-input-container>

		<md-button class="md-button md-raised md-primary" ng-click="sendMessage(dataModel,Form)" can-save ng-disabled="!canSave(Form)" >
		    <span ng-if="dataSaved">Submit</span>
		    <span ng-if="!dataSaved">Submiting...</span>
		</md-button>

		</form>
		</div>
	</div>

</section>
