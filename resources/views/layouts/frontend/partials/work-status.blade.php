<section class="io-work-status">
    <div class="bg-light-gray center section-md-banner">
        <h2>Fun Facts in this month</h2>

        <div class="container">
            <div class="mdl-grid">
                <div class="mdl-cell--3-col-desktop">
                    <p id="code">13,000</p>
                    <span><i class="fa fa-code"></i></span>
                    <p>Line of Codes</p>
                </div>
                <div class="mdl-cell--3-col-desktop">
                    <p id="workingHr">360</p>
                    <span><i class="fa fa-clock-o"></i></span>
                    <p>Working Hours</p>
                </div>
                <div class="mdl-cell--3-col-desktop">
                    <p id="cupsOfCoffee">132</p>
                    <span><i class="fa fa-coffee"></i></span>
                    <p>Cups of Coffee</p>
                </div>
                <div class="mdl-cell--3-col-desktop">
                    <p id="happySmile">300</p>
                    <span><i class="fa fa-meh-o"></i></span>
                    <p>Happy Smiles</p>
                </div>
            </div>
        </div>
    </div>
</section>