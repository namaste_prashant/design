<section class="io-intro">
	<div class="image-about">
		 <div style="background-image:url('images/banner/2.jpg');height:320px;background-size:cover;background-position-y:15%"></div>
	</div>
    <div class="bg-light-green">
        <div class="container center section-banner">
            <h1>Handpicked from our warehouse</h1>
            <p>Our clients speak about our work more than words could.</p>
        </div>
    </div>
    <div class="floating--button">
		<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent">
  			<i class="material-icons">expand_more</i>
		</button>
    </div>
</section>