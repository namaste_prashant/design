<section class="io-banner-web-design">
	<div class="bg-creamy">
		<div class="container center section-banner">
            <h1 class="text-bd">User Friendly Web Design</h1>
			<p>We challenged ourselves to create a visual language for our users that synthesizes the classic principles of good design with the innovation and possibility of technology and science. </p>
        	<br>
        	<a href="#contact-us" class="md-button md-raised md-custom-center">Start a Project</a>
        </div>
	</div>
	 <div class="image-about">
        <div style="background-image:url('images/banner/14.jpg');height:420px;background-size:cover;background-position-y:45%"></div>
    </div>
</section>