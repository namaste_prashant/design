<section class="io-technology">
    <div class="bg-dark-gray">
        <div class="container section-30-banner">
            <h1>Technology</h1>
            <p>Our technical expertise is not just limited in one field. People at Ideas Offshore have expertise in the following fields.</p>
            <md-content class="md-padding" layout="row" layout-wrap="" layout-align="center start" layout-xs="column">
                 <ul class="mid-center">
                @foreach($technologies as $technology)
                <li>
                    <img src="{{$technology->logo_image}}" /> 
                    <md-tooltip md-direction="bottom"><p>{{$technology->title}}</p></md-tooltip>
                </li>
                 @endforeach
                 </ul>
            </md-content>
        </div>
    </div>
</section>