<!doctype>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ideas Offshore</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png" />

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/material.min.css') }}">

<link rel="stylesheet" href="/css/admin-styles.css">
<link rel="stylesheet" href="/bower_components/angular-material/angular-material.css">

<link rel="stylesheet" href="/bower_components/textAngular/dist/textAngular.css">
<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,300">

<script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript" src="/bower_components/angular/angular.js"></script>
<script type="text/javascript" src="/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script type="text/javascript" src="/bower_components/angular-resource/angular-resource.js"></script>
<script type="text/javascript" src="/bower_components/angular-animate/angular-animate.js"></script>
<script type="text/javascript" src="/bower_components/angular-aria/angular-aria.js"></script>
<script type="text/javascript" src="/bower_components/angular-material/angular-material.js"></script>

<script type="text/javascript" src="/bower_components/angular-sanitize/angular-sanitize.js"></script>
<script type="text/javascript" src="/bower_components/ng-file-upload/ng-file-upload.js"></script>
<script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

<script type="text/javascript" src='/bower_components/textAngular/dist/textAngular-rangy.min.js'></script>
<script type="text/javascript" src='/bower_components/textAngular/dist/textAngular-sanitize.min.js'></script>
<script type="text/javascript" src='/bower_components/textAngular/dist/textAngular.min.js'></script>

<script type="text/javascript" src="/admins/src/app/app.js"></script>
<script type="text/javascript"  src="/admins/src/common/directives/header/header.js"></script>
<script type="text/javascript"  src="/admins/src/common/directives/sidebar/sidebar.js"></script>
<script type="text/javascript"  src="/admins/src/common/directives/slide/slide.js"></script>
<script type="text/javascript"  src="/admins/src/common/directives/formButton/formButton.js"></script>

<script type="text/javascript" src="/admins/src/common/resources/page.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/post.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/photo.js"></script>

<script type="text/javascript" src="/admins/src/common/resources/gallery.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/section.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/portfolio.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/page-section.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/setting.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/portfolio_category.js"></script>
<script type="text/javascript" src="/admins/src/common/resources/technology.js"></script>


<script type="text/javascript" src="/admins/src/common/services/rjServices.js"></script>
<script type="text/javascript" src="/admins/src/common/directives/rjDirective.js"></script>

<script type="text/javascript" src="/admins/src/app/dashboard/dashboard.js"></script>
<script type="text/javascript" src="/admins/src/app/page/page.js"></script>
<script type="text/javascript" src="/admins/src/app/page/page-view/page-view.js"></script>
<script type="text/javascript" src="/admins/src/app/page/section/page-section.js"></script>
<script type="text/javascript" src="/admins/src/app/section/section.js"></script>
<script type="text/javascript" src="/admins/src/app/post/post.js"></script>
<script type="text/javascript" src="/admins/src/app/gallery/gallery.js"></script>
<script type="text/javascript" src="/admins/src/app/gallery/image/image.js"></script>
<script type="text/javascript" src="/admins/src/app/portfolio/portfolio.js"></script>
<script type="text/javascript" src="/admins/src/app/setting/setting.js"></script>


<script type="text/javascript" src="/admins/src/app/page/page-content/page-content.js"></script>
<script type="text/javascript" src="/admins/src/app/page/page-cover-image/page-cover-image.js"></script>
<script type="text/javascript" src="/admins/src/app/page/page-meta/page-meta.js"></script>

<script type="text/javascript" src="/admins/src/app/portfolio_category/portfolio_category.js"></script>
<script type="text/javascript" src="/admins/src/app/technology/technology.js"></script>

<?php

if (!Auth::guest()) {
    $login = 'true';
    $user  = Auth::user()->toArray();
} else {
    $login = 'false';
    $user  = null;
}

?>

<script type="text/javascript">

    var base_url    =   '<?php echo url(); ?>',
        user        =   <?php echo json_encode($user) ?>,
        login       =   <?php echo $login ?>;

</script>

  </head>

  <body class="mdl-demo mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">

    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header"
    ng-app="app" ng-controller="BaseController">

        <adminsidebar>
        </adminsidebar>

        <adminheader>
        </adminheader>

        <main class="mdl-layout__content mdl-color--grey-100">
            <ui-view>
            </ui-view>
        </main>

    </div>

    <script src="{!! asset('js/material.min.js') !!}"></script>

  </body>

</html>





