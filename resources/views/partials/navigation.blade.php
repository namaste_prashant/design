<?php

echo output($pages);

function output($pages)
{
    $string = '';

    foreach ($pages as $i => $page) {
        $active = Request::is($page->slug) ? "is-active" : "";
        $string .= '<li class="';
        $string .= $active;
        $string .= " ";
        $string .= count($page->children) ? ($page->isChild() ? 'dropdown-submenu' : 'drpmnu dropdown--exit') : '';
        $string .= '">';
        $string .= '<a href="';
        $string .= $page->slug;
        $string .= '" class= "menu-button "><span class="icon '. $page->slug. '"></span>';
        $string .= $page->page_title;

        if (count($page->children)) {

            $string .= '<span class="material-icons pull-right';
            $string .= $page->isChild() ? 'right' : '';
            $string .= '">arrow_drop_down</span>';
        }

        $string .= '</a>';

        if (count($page->children)) {

            $string .= '<ul class="dropdown-menu" role="menu">';

            $string .= output($page->children);

            $string .= '</ul>';
        }

        $string .= '</li>';
    }

    return $string;
}
