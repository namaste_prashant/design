<?php

echo responsive($pages);

function responsive($pages)
{
    $string = '';

    foreach ($pages as $i => $page) {
        $active = Request::is($page->slug) ? "is-active" : "";
        $string .= '<li class="nav-item ';
        $string .= $active;
        $string .= " ";
        $string .= count($page->children) ? ($page->isChild() ? 'dropdown-a' : 'a dropdown--a') : '';
        $string .= '">';
        $string .= '<a href="';
        $string .= $page->slug;
        $string .= '">';
        $string .= $page->page_title;

        if (count($page->children)) {

            $string .= '<span class="material-icons pull-right';
            $string .= $page->isChild() ? 'right' : '';
            $string .= '">arrow_drop_down</span>';
        }

        $string .= '</a>';

        if (count($page->children)) {

            $string .= '<ul class="nav" role="menu">';

            $string .= responsive($page->children);

            $string .= '</ul>';
        }

        $string .= '</li>';
    }

    return $string;
}
