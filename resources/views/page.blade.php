@extends('layouts.frontend')
@section('title',$page->page_title)


@section('content')

	<!-- If page has sections include them. -->
	@foreach ($page->sections as $section)

		@if (view()->exists('layouts/frontend/partials/'.$section->title))

			@include('layouts/frontend/partials/'.$section->title)

		@endif

		<!--
		  If section's title is content,
		  and has view render the view.
		  else render page conten.
		 -->
		@if($section->title=='content')

			@if($page->view)

				{!! $page->view->render() !!}

			@else

				{!! $page->page_content !!}

			@endif

		@endif

	@endforeach

@endsection
