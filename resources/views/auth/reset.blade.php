
@extends('layouts.admin')
@section('content')

<main class="mdl-components_pages">
<section class="mdl-components_page mdl-grid">
    <div class="mdl-cell mdl-cell--12-col">
        <div class="components-title">
            <div class="mdl-center mdl-card mdl-shadow--2dp">
                <div class="mdl-card__title mdl-card--expand">
                    <h2 class="mdl-card__title-text">Login Details</h2>
                </div>

                    @if($errors->any())

                        <div class="alert alert-danger">
                            <strong>We found some errors!</strong>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif

                    {!! Form::open() !!}
                    {!! Form::hidden('token',$token) !!}

                    <div class="login-box mdl-card__supporting-text">

                        <div class="mdl-textfield mdl-js-textfield">

                            {!! Form::text('email',null, ['class'=>'mdl-textfield__input']) !!}

                            <label class="mdl-textfield__label" for="userName">Email</label>

                        </div>

                        <div class="mdl-textfield mdl-js-textfield">

                            {!! Form::password('password',['class'=>'mdl-textfield__input']) !!}

                            <label class="mdl-textfield__label" for="password">Password</label>

                        </div>

                        <div class="mdl-textfield mdl-js-textfield">

                            {!! Form::password('password_confirmation',['class'=>'mdl-textfield__input']) !!}

                            <label class="mdl-textfield__label" for="password">Confirm Password</label>

                        </div>

                    </div>
                    <div class="mdl-card__actions mdl-card--border">

                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                        Reset Password
                        </button>
                        {!! Form::close() !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</main>
@stop
@section('head')
<link rel="stylesheet" href="{{ URL::asset('css/login-styles.css') }}">
@stop
