@foreach ($posts as $post)
	<article>
		<h2><a href="{{ url('article', [$post->id,$post->slug])}}">{{$post->title}}</a></h2>
		<p>{!! $post->body !!}</p>
	</article>
@endforeach

{!! $posts->render() !!}
