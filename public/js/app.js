    
(function(init){
	 dropDownHoverMenu();
     responsiveMenu();
    stickyHead();
})(window.init || (window.init ={}));

function dropDownHoverMenu(){

 // Show and hide submenu in hovered and hoverout.clear
       var classname = document.getElementsByClassName("drpmnu");

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener("mouseover", hovered, false);
            classname[i].addEventListener("mouseout", hoveredOut, false);
        }

        function hovered(e) {
            this.className="drpmnu dropdown--show";
        }

        function hoveredOut(e) {
         this.className="drpmnu dropdown--exit";
        }
}

function responsiveMenu(){
    
    function activateMenu(e){            
        $('#res-menu').addClass("is-active");
        $('body').addClass("scroll-off");
    }

    function deactivateMenu(e){
            $('#res-menu').removeClass("is-active");
            $('body').removeClass("scroll-off");
    }    
 
        $( "#sm-menu" ).bind( "click", function(e) {        
            activateMenu(e);
        });
   

        $("section").bind('click','el',function(e){   
            e.stopPropagation();
            if(e.target.className=="material-icons" || e.target.className=="basement-nav" || e.target.className=="basement-header"){
                return;
            }        
            deactivateMenu(e);      
        });
        $(window).resize(deactivateMenu);
}

function stickyHead() {
    var oldPageYOffset = 0;
    $(window).scroll(function(){
        if (window.pageYOffset != oldPageYOffset)
        {
            $('.navbar').addClass("sticky");
        } else {
             $('.navbar').removeClass("sticky");
        }
    });
}