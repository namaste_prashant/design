angular.module('portfolio', [
    'resources.portfolio',
    'resources.portfolio_category',
    'resources.technology',
    'resources.gallery'
]);

angular.module('portfolio').controller('PortfolioListController', PortfolioListController);

function PortfolioListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, PortfolioFactory) {

    $scope.getData = getData;
    $scope.remove = remove;
    $scope.CreateForm = CreateForm;
    $scope.changeStatus = changeStatus;
    $scope.dataLoaded = false;
    $scope.param = {};
    $scope.pageChanged = pageChanged;
    getData();

    function pageChanged(page) {
        $scope.param.currentPage = $scope.currentPage;
        $scope.getData($scope.param);
    };


    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        PortfolioFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            //$scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {
        ConfirmFactory.show($event, 'You really want to remove this !!').then(function() {
            PortfolioFactory.remove(id).then(function(repsonse) {
                $scope.getData($scope.param);
            });
        });
    }

    // Create form for create and Save.
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/portfolio/form.tpl.html',
            contrl = SavePortfolioController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.mode = "edit";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                $scope.getData($scope.param);
            });
        } else {
            data.mode = "add";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function(response) {
                $scope.getData($scope.param);
            });

        }
    }

    // Change the status.
    function changeStatus(data) {
        PortfolioFactory.save(data);
    }

}

function SavePortfolioController(data, $scope, $mdDialog, PortfolioFactory, $mdToast, data, $state, PortfolioCategoryFactory, Upload, TechnologyFactory, GalleryFactory) {

    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : {};
    if (!data.dataModel) {
        $scope.dataModel.categories = [];
        $scope.dataModel.technologies = [];
    }
    $scope.mode = data.mode;
    $scope.dataSaved = true;
    $scope.autocompleteDemoRequireMatch = true;
    $scope.transformChip = transformChip;
    $scope.selectedItem = null;
    $scope.selectedTechnology = null;
    $scope.searchText = null;
    $scope.querySearch = querySearch;
    $scope.querySearchTechnology = querySearchTechnology;
    $scope.categories = [];
    $scope.technologies = [];
    init();

    // Do initial setup.
    function init() {

        PortfolioCategoryFactory.list().then(function(response) {
            $scope.categoryList = response;
            return $scope.categoryList.map(function(veg) {
                veg._lowertitle = veg.title.toLowerCase();
                $scope.categories.push(veg);
            });
        });

        TechnologyFactory.list().then(function(response) {
            $scope.technologyList = response;
            return $scope.technologyList.map(function(veg) {
                veg._lowertitle = veg.title.toLowerCase();
                $scope.technologies.push(veg);
            });
        });

        GalleryFactory.list().then(function(response) {
            $scope.galleryList = response;
        })
    }

    /**
     * Return the proper object when the append is called.
     */
    function transformChip(chip) {
        // If it is an object, it's already a known chip
        if (angular.isObject(chip)) {
            return chip;
        }
        // Otherwise, create a new one
        return {
            title: chip,
            type: 'new'
        }
    }

    /**
     * Search for categoires.
     */
    function querySearch(query) {
        var results = query ? $scope.categories.filter(createFilterFor(query)) : [];
        return results;
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(category) {
            return (category._lowertitle.indexOf(lowercaseQuery) === 0);
        };
    }

    /**
     * Create filter function for a query string
     */
    function querySearchTechnology(query) {
        var results = query ? $scope.technologies.filter(createFilterFor(query)) : [];
        return results;
    }


    function save(data) {
        $scope.dataSaved = false;
        if (data.file) {
            data.logo_image = data.file.name;
        }
        var url = '/admin/portfolio';
        Upload.upload({
            url: url,
            data: {
                file: data.file,
                data
            }
        }).then(function(response) {
            $scope.dataSaved = true;
            $mdDialog.hide(response);
        }, function(response) {

        });
    }
}
