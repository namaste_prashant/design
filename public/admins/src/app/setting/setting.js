angular.module('setting', [
    'resources.setting',
]);

angular.module('setting').controller('SettingController', SettingController);

function SettingController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, SettingFactory, Upload) {

    $scope.getData = getData;
    $scope.dataLoaded = false;
    $scope.updateItem = updateItem;
    $scope.updating = {};
    getData();


    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        SettingFactory.getDataList(param).then(function(response) {
            $scope.company = response.data;
            $scope.dataLoaded = true;
        });
    }

    function updateItem($event, feild, data) {
        if ($scope.updating[feild] == undefined) {
            $scope.updating[feild] = true;
            return;
        }

        if ($scope.updating[feild]) {
            // If file is being uploading get the name and
            //  set it to in model attribute.
            if (data.file) {
                data.logo = data.file.name;
            }
            var url = '/admin/setting';
            Upload.upload({
                url: url,
                data: {
                    file: data.file,
                    data
                }
            }).then(function(response) {
                // reload the data after successfully uploaded the file.
                $scope.getData();
                $scope.updating[feild] = false;

            }, function(response) {

            });
        }

        if (!$scope.updating[feild]) {
            $scope.updating[feild] = true;
        }
    }
}
