angular.module('image', [
    'resources.photo',
    'slide'
]);

angular.module('image').controller('ImageListController', ImageListController);

function ImageListController($scope, $stateParams, ModalFactory, PhotoFactory, ConfirmFactory) {
    $scope.getData = getData;
    $scope.addImage = addImage;
    $scope.remove = remove;
    $scope.gallery_id = $stateParams.id;
    $scope.dataLoaded = true;
    $scope.param = {
        imageable_id: $scope.gallery_id
    };
    $scope.showItem = showItem;
    $scope.changeStatus = changeStatus;
    getData($scope.param);


    // Change the status.
    function changeStatus(data, $event) {
        if ($event) {
            $event.preventDefault();
            $event.stopPropagation();
        }
        PhotoFactory.save(data);
    }

    // show images.
    function showItem($event, $index) {
        $event.preventDefault();
        var templateUrl = '/admins/src/app/gallery/image/show-item.tpl.html',
            contrl = ShowItemController,
            data = {
                dataList: $scope.dataList,
                index: $index
            }

        ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
            $scope.getData($scope.param);
        });
    }

    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        PhotoFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Create form for create and Save.
    function addImage($event) {
        var templateUrl = '/admins/src/app/gallery/image/form.tpl.html',
            contrl = AddImageController,
            data = {
                id: $scope.gallery_id
            }

        ModalFactory.showModal($event, contrl, templateUrl, data)
            .then(function() {
                $scope.getData($scope.param);
            });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {
        $event.preventDefault();
        $event.stopPropagation();
        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                PhotoFactory.remove(id).then(function(repsonse) {
                    // $scope.getData($scope.param);
                    $scope.dataList.splice($index, 1);
                });
            });
    }


}

function AddImageController($scope, data, Upload, $mdDialog, $state) {
    $scope.gallery_id = data.id;
    $scope.save = save;
    $scope.dataSaved = true;

    // Upload image in gallery.
    function save(data) {
        $scope.dataSaved = false;
        data.imageable_id = $scope.gallery_id;
        data.imageable_type = 'Gallery';
        if (data.file) {
            data.path = data.file.name;
        }
        var url = '/admin/photo';
        Upload.upload({
            url: url,
            data: {
                file: data.file,
                data
            }
        }).then(function(response) {
            $scope.dataSaved = true;
            $mdDialog.hide(response);
        }, function(response) {

        });
    }

}

function ShowItemController($scope, data) {
    $scope.dataList = data.dataList;
    $scope.index = data.index;
}
