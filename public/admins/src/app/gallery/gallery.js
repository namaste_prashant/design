angular.module('gallery', [
    'resources.gallery',
    'image'
]);

angular.module('gallery').controller('GalleryListController', GalleryListController);

function GalleryListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, GalleryFactory, $state) {
    // Methods for controller.
    $scope.getData = getData;
    $scope.remove = remove;
    $scope.add = add;
    $scope.dataLoaded = false;
    getData();


    function add(data) {
        GalleryFactory.save(data).then(function(response) {
            getData();
        });
    }

    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        GalleryFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {
        $event.preventDefault();
        $event.stopPropagation();
        ConfirmFactory.show($event, 'You really want to remove this !!').then(function() {
            GalleryFactory.remove(id).then(function(repsonse) {
                $scope.dataList.splice($index, 1);
            });
        });
    }
}
