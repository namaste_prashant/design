angular.module('technology', [
    'resources.technology'
]);

angular.module('technology').controller('TechnologyListController', TechnologyListController);

function TechnologyListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, TechnologyFactory) {

    $scope.getData = getData;
    $scope.remove = remove;
    $scope.CreateForm = CreateForm;
    $scope.changeStatus = changeStatus;
    $scope.dataLoaded = false;
    $scope.param = {};
    $scope.pageChanged = pageChanged;
    getData();


    function pageChanged(page) {
        $scope.param.currentPage = $scope.currentPage;
        $scope.getData($scope.param);
    };


    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        TechnologyFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {

        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                TechnologyFactory.remove(id).then(function(repsonse) {
                    $scope.getData($scope.param);
                    //$scope.courses.splice($index, 1);
                });
            });
    }

    // Create form for create and Save.
    function CreateForm($event, dataModel) {        
        var templateUrl = 'admins/src/app/technology/form.tpl.html',
            contrl = SaveTechnologyController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.mode = "edit";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                    $scope.getData($scope.param);
                });
        } else {
            data.mode = "add";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function(response) {
                    $scope.getData($scope.param);
                });

        }
    }

    // Change the status.
    function changeStatus(data) {
        TechnologyFactory.save(data);
    }

}

function SaveTechnologyController(data, $scope, $mdDialog, TechnologyFactory, $mdToast, data, $state,Upload) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.mode = data.mode;
    $scope.dataSaved = true;

    function save(data) {
        $scope.dataSaved = false;
        if (data.file) {
            data.logo_image = data.file.name;
        }
        var url = '/admin/technology';
        Upload.upload({
            url: url,
            data: {
                file: data.file,
                data
            }
        }).then(function(response) {
            $scope.dataSaved = true;
            $mdDialog.hide(response);
        }, function(response) {

        });
    }

}