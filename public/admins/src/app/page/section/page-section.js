angular.module('page').controller('PageSectionListController', PageSectionListController);


function PageSectionListController($scope, $rootScope, SectionFactory, $stateParams, PageFactory, PageSectionFactory, ConfirmFactory) {

    $scope.addSection = addSection;
    $scope.page_id = $scope.$stateParams.id;
    $scope.getData = getData;
    $scope.dataLoaded = false;
    $scope.pageChanged = pageChanged;
    $scope.param = {
        page_id: $scope.page_id
    };
    $scope.remove = remove;
    getData($scope.param);
    $scope.changeStatus = changeStatus;

    function remove(id, $index, $event) {
        $event.preventDefault();
        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                PageSectionFactory.remove(id).then(function(repsonse) {
                    $scope.dataList.splice($index, 1);
                });
            });
    }

    function pageChanged(page) {
        $scope.param.currentPage = $scope.currentPage;
        $scope.getData($scope.param);
    };

    $rootScope.$on('RJ-DRAG-START', function(obj, scope) {
        $scope.sourceIndex = scope.$index;
        $scope.sourceData = scope.data;
    });

    $rootScope.$on('RJ-DROP-START', function(obj, scope) {
        $scope.dataList.splice($scope.sourceIndex, 1);
        $scope.dataList.splice(scope.$index, 0, $scope.sourceData);
        $scope.$apply();
        updatePageSection();
    });

    function getData(param) {
        $scope.dataList = [];
        PageSectionFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });

        // Get list of sections.
        SectionFactory.list().then(function(response) {
            $scope.sectionList = response;
        });

    }

    // Add section to the page.
    function addSection($event, section_id) {
        $event.preventDefault();
        var data = {
            page_id: $scope.page_id,
            section_id: section_id
        };
        PageSectionFactory.save(data).then(function(response) {
            $scope.getData($scope.param);
        });
    }

    // Update page_sections to reorder list.
    function updatePageSection() {
        $scope.updateStatus = false;        
        PageSectionFactory.updatePageSections($scope.dataList).then(function(response) {
            if (response) {
                $scope.updateStatus = true;
            }
        });
    }

    // Change the status.
    function changeStatus(data) {
        PageSectionFactory.save(data);
    }
}