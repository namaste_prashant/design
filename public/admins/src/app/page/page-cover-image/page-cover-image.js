
angular.module('post').controller('PageCoverImageController', PageCoverImageController);

function PageCoverImageController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory,PageFactory) {    

    $scope.CreateForm = CreateForm;
    
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/page/page-cover-image/form.tpl.html',
            contrl = SavePageCoverImageController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.title = "Update Cover Image";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                $scope.getData();
            });
        } 
    }

}

function SavePageCoverImageController(data, $scope, $mdDialog, PageFactory, $mdToast, data ,$state,Upload) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.title = data.title;
    $scope.dataSaved = true;



    function save(data) {

        $scope.dataSaved = false;
        if (data.file) {
            data.cover_image = data.file.name;
        }
        var url = '/admin/pages';
        Upload.upload({
            url: url,
            data: {
                file: data.file,
                data
            }
        }).then(function(response) {
            $scope.dataSaved = true;
            $mdDialog.hide(response);
        }, function(response) {

        });

        // PageFactory.save(data).then(function(response) {
        //     $mdDialog.hide(response);
        // });
    }
}