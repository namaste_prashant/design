
angular.module('post').controller('PageContentController', PageContentController);

function PageContentController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory,PageFactory) {    

    $scope.CreateForm = CreateForm;
    
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/page/page-content/form.tpl.html',
            contrl = SavePageContentController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.title = "Update Page Content";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                $scope.getData();
            });
        } 
    }

}

function SavePageContentController(data, $scope, $mdDialog, PageFactory, $mdToast, data ,$state) {
    $scope.save = save;    
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.title = data.title;
    $scope.dataSaved = true;
    
    function save(data) {
        $scope.dataSaved = false;
        PageFactory.save(data).then(function(response) {
                $scope.dataSaved = true;
            $mdDialog.hide(response);
        });
    }
}