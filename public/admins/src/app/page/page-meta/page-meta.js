
angular.module('post').controller('PageMetaController', PageMetaController);

function PageMetaController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory,PageFactory) {    

    $scope.CreateForm = CreateForm;
    
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/page/page-meta/form.tpl.html',
            contrl = SavePageMetaController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.title = "Update Meta data";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                $scope.getData();
            });
        } 
    }

}

function SavePageMetaController(data, $scope, $mdDialog, PageFactory, $mdToast, data ,$state) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.title = data.title;
    $scope.dataSaved = true;
    
    function save(data) {
        $scope.dataSaved = false;
        PageFactory.save(data).then(function(response) {
            $scope.dataSaved = true;
            $mdDialog.hide(response);
        });
    }
}