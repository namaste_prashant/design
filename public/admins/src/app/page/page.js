angular.module('page', [
    'resources.page',
    'resources.section',
    'resources.pageSection'
]);

angular.module('page').controller('PageListController', PageListController);

function PageListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, PageFactory) {

    $scope.getData = getData;
    $scope.remove = remove;
    $scope.CreateForm = CreateForm;
    $scope.changeStatus = changeStatus;
    $scope.dataLoaded = false;
    $scope.pageChanged = pageChanged;
    $scope.param = {};
    getData();

    function pageChanged(page) {
        $scope.param.currentPage = $scope.currentPage;
        $scope.getData($scope.param);
    };

    //Retrive all dataList.         
    function getData(param) {
        $scope.pages = [];
        PageFactory.getDataList(param).then(function(response) {
            $scope.pages = response.data;
            $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {

        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                PageFactory.remove(id).then(function(repsonse) {
                    $scope.getData($scope.param);
                    // $scope.pages.splice($index, 1);
                });
            });
    }

    // Create form for create and Save.
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/page/form.tpl.html',
            contrl = SavePageController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.mode = "edit";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {
                $scope.getData($scope.param);
            });
        } else {
            data.mode = "add";
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function(response) {
                $scope.getData($scope.param);
            });

        }
    }

    // Change the status.
    function changeStatus(data) {
        PageFactory.save(data);
    }

}

function SavePageController(data, $scope, $mdDialog, PageFactory, $mdToast, data, $state) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.mode = data.mode;
    $scope.dataSaved = true;

    PageFactory.list().then(function(data) {
        $scope.pages = data;
    });

    PageFactory.getPageTemplates().then(function(data) {
        $scope.templates = data;
    });

    function save(data) {
        //$scope.dataSaved = false;
        PageFactory.save(data).then(function(response) {
            $mdDialog.hide(response);
           // $scope.dataSaved = true;
        });
    }
}