


angular.module('page').controller('PageViewController', PageViewController);

function PageViewController($scope, PageFactory, $stateParams, ModalFactory) {
    $scope.dataLoaded = false;
    $scope.page_id = $stateParams.id;   
    $scope.getData=getData;
    getData();

    function getData(){
		PageFactory.getDataItem($scope.page_id).then(function(data) {  			
		    $scope.page = data;
		    $scope.dataLoaded = true;
		});    	
    }
	    
}
