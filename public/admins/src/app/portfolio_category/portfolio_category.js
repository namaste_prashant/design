angular.module('portfolio_category', [
    'resources.portfolio_category'
]);

angular.module('portfolio_category').controller('PortfolioCategoryListController', PortfolioCategoryListController);

function PortfolioCategoryListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory, PortfolioCategoryFactory) {

    $scope.getData = getData;
    $scope.remove = remove;
    $scope.CreateForm = CreateForm;
    $scope.changeStatus = changeStatus;
    $scope.dataLoaded = false;
    $scope.param = {};
    $scope.pageChanged = pageChanged;
    getData();


    function pageChanged(page) {
        $scope.param.currentPage = $scope.currentPage;
        $scope.getData($scope.param);
    };


    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        PortfolioCategoryFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {

        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                PortfolioCategoryFactory.remove(id).then(function(repsonse) {
                    $scope.getData($scope.param);
                    //$scope.courses.splice($index, 1);
                });
            });
    }

    // Create form for create and Save.
    function CreateForm($event, dataModel) {        
        var templateUrl = 'admins/src/app/portfolio_category/form.tpl.html',
            contrl = SavePortfolioCategoryController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.mode = "edit";
            ModalFactory.showModal($event, contrl, templateUrl, data)
                .then(function() {

                });
        } else {
            data.mode = "add";
            ModalFactory.showModal($event, contrl, templateUrl, data)
                .then(function(response) {
                    $scope.getData($scope.param);
                });

        }
    }

    // Change the status.
    function changeStatus(data) {
        PortfolioCategoryFactory.save(data);
    }

}

function SavePortfolioCategoryController(data, $scope, $mdDialog, PortfolioCategoryFactory, $mdToast, data, $state) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.mode = data.mode;


    function save(data) {        
        PortfolioCategoryFactory.save(data).then(function(response) {
            $mdDialog.hide(response);
            $state.go($state.$current, null, {
                reload: true
            });
        });
    }
}