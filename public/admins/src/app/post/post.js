angular.module('post', [
    'resources.post'
]);

angular.module('post').controller('PostListController', PostListController);

function PostListController($scope, $mdDialog, $mdMedia, ConfirmFactory, ModalFactory,PostFactory) {

   // Methods for controller.
    $scope.getData = getData;
    $scope.remove = remove;
    $scope.CreateForm = CreateForm;
    $scope.changeStatus=changeStatus;
    // // Variables for scope.
    $scope.dataLoaded = false;
    // $scope.param = {};
    getData();
    

    // // When page changed from pagination button.
    // // Set the currentPage and reload the datalist.
    // $scope.pageChanged = function(page) {
        
    //     $scope.param.currentPage = $scope.currentPage;
    //     $scope.getData($scope.param);
    // };



    //Retrive all dataList.         
    function getData(param) {
        $scope.dataList = [];
        PostFactory.getDataList(param).then(function(response) {
            $scope.dataList = response.data;
            // $scope.totalItems = response.total;
            $scope.dataLoaded = true;
        });
    }

    // Remove the dataItem form the dataList.
    function remove(id, $index, $event) {

        ConfirmFactory.show($event, 'You really want to remove this !!')
            .then(function() {
                PostFactory.remove(id).then(function(repsonse) {
                    $scope.getData($scope.param);
                    //$scope.courses.splice($index, 1);
                });
            });
    }

    // Create form for create and Save.
    function CreateForm($event, dataModel) {
        var templateUrl = 'admins/src/app/post/form.tpl.html',
            contrl = SavePostController,
            data = {
                dataModel: dataModel
            };

        if (dataModel) {
            data.mode = "edit";
            ModalFactory.showModal($event, contrl, templateUrl, data)
                .then(function() {

                });
        } else {
            data.mode = "add";
            ModalFactory.showModal($event, contrl, templateUrl, data)
                .then(function(response) {
                    $scope.getData($scope.param);
                });

        }
    }

    // Change the status.
    function changeStatus(data) {        
        PostFactory.save(data);
    }

}

function SavePostController(data, $scope, $mdDialog, PostFactory, $mdToast, data ,$state) {
    $scope.save = save;
    $scope.dataModel = data.dataModel ? data.dataModel : null;
    $scope.mode = data.mode;

    
    function save(data) {
        PostFactory.save(data).then(function(response) {
            $mdDialog.hide(response);
            $state.go($state.$current, null, { reload: true });
        });
    }
}