/**
 *  Root module for application.
 */
angular.module('app', [
    'ui.router',
    'dashboard',
    'ngMaterial',
    'rjDirective',
    'ngSanitize',
    'gallery',
    'page',
    'post',
    'portfolio',
    'ngFileUpload',
    'textAngular',
    'section',
    'ui.bootstrap',
    'setting',
    'portfolio_category',
    'technology'
]);

/**
 *  Calling order of angularjs app.
 *  1-> app.config()
 *  2-> app.run()
 *  3-> directive's compile functions (if found in the dom)
 *  4-> app.controller()
 *  5-> directive's link functions (if found)
 *  
 */

angular.module('app').run(['$rootScope', '$state', '$stateParams',
    function($rootScope, $state, $stateParams) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        // This is will add this functios on every string.
        String.prototype.capitalizeFirstLetter = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
    }

]);



// Register controller.
angular.module('app').controller('BaseController', BaseController);


// Base Controller for in root scope.
function BaseController($scope) {

    $scope.login = login;
    $scope.user = user;


    // Initial parameters for pagination.
    $scope.maxSize = 5;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
}


// Define route in config .
angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$interpolateProvider',
    function($stateProvider, $urlRouterProvider, $interpolateProvider) {

        $interpolateProvider.startSymbol('[[').endSymbol(']]');

        $urlRouterProvider.otherwise('/dashboard');
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'admins/src/app/dashboard/dashboard.tpl.html',
                controller: 'DashboardController',
            })
            .state('page', {
                url: '/page',
                templateUrl: 'admins/src/app/page/page-list.tpl.html',
                controller: 'PageListController',
            })
            .state('page-view', {
                url: '/page-view/:id',
                templateUrl: 'admins/src/app/page/page-view/page.view.tpl.html',
                controller: 'PageViewController',
            })
            .state('post', {
                url: '/post',
                templateUrl: 'admins/src/app/post/post-list.tpl.html',
                controller: 'PostListController',

            })
            .state('portfolio', {
                url: '/portfolio',
                templateUrl: 'admins/src/app/portfolio/portfolio-list.tpl.html',
                controller: 'PortfolioListController',
            })
            .state('gallery', {
                url: '/gallery',
                templateUrl: '/admins/src/app/gallery/gallery-list.tpl.html',
                controller: 'GalleryListController',
            })
            .state('gallery.image', {
                url: '/gallery-image/:id',
                templateUrl: '/admins/src/app/gallery/image/image-list.tpl.html',
                controller: 'ImageListController',
            })
            .state('section', {
                url: '/section',
                templateUrl: 'admins/src/app/section/section-list.tpl.html',
                controller: 'SectionListController',
            })
            .state('setting', {
                url: '/setting',
                templateUrl: 'admins/src/app/setting/setting.tpl.html',
                controller: 'SettingController',
            })
            .state('portfolio_category', {
                url: '/portfolio_category',
                templateUrl: 'admins/src/app/portfolio_category/portfolio_category-list.tpl.html',
                controller: 'PortfolioCategoryListController',
            })
            .state('technology', {
                url: '/technology',
                templateUrl: 'admins/src/app/technology/technology-list.tpl.html',
                controller: 'TechnologyListController',
            });            
    }
]);