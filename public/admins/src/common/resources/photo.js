/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.photo', ['ngResource', 'ngMaterial', 'rjServices']);
angular.module('resources.photo').factory('Photo', Photo);
angular.module('resources.photo').factory('PhotoFactory', PhotoFactory);

function Photo(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/photo/:id');
}

function PhotoFactory(Photo, BaseModelFactory, $q, $http) {
    var fac = {},
        res = Photo;

    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }

    function getDataList(param) {
        return BaseModelFactory.getDataList(res, param);
    }

    function save(data) {
        return BaseModelFactory.save(res, data);
    }

    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    return fac;
}