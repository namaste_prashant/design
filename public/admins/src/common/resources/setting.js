/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.setting', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.setting').factory('Setting', Setting);
angular.module('resources.setting').factory('SettingFactory', SettingFactory);


function Setting(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/setting/:id');
}


function SettingFactory(Setting, BaseModelFactory, $q, $http) {
    var fac = {},
        res = Setting;
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;    

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(res, param);
    }


    function save(data) {
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    return fac;
}