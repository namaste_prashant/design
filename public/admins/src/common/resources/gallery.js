/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.gallery', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.gallery').factory('Gallery', Gallery);
angular.module('resources.gallery').factory('GalleryFactory', GalleryFactory);


function Gallery(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/gallery/:id');
}


function GalleryFactory(Gallery, BaseModelFactory, $q, $http) {
    var fac = {},
        res = Gallery;

    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.list=list;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(res, param);
    }


    function save(data) {
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    function list() {        
        var deferred = $q.defer();
        $http.get("/admin/gallery_list")
            .then(function(response) {                
                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }

    return fac;
}