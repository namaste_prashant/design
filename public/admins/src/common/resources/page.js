/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.page', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.page').factory('Page', Page);
angular.module('resources.page').factory('PageFactory', PageFactory);


function Page(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/pages/:id');
}


function PageFactory(Page, BaseModelFactory, $q, $http, AlertFactory) {
    var fac = {};
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.list = list;
    fac.getPageTemplates = getPageTemplates;
    fac.addSection = addSection;
    fac.removeSection = removeSection;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(Page, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(Page, param);
    }


    function save(data) {
        return BaseModelFactory.save(Page, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(Page, id);
    }

    function list() {
        var deferred = $q.defer();
        $http.get("admin/page_list")
            .then(function(response) {
                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }

    function getPageTemplates() {
        var deferred = $q.defer();
        $http.get("admin/get_page_templates")
            .then(function(response) {

                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }


    function addSection(data) {

        var deferred = $q.defer();

        $http({
                url: '/admin/add_section',
                method: "POST",
                data: data
            })
            .then(function(response) {
                if (response.data.success) {
                    deferred.resolve(response.data.data);
                } else {
                    AlertFactory.show($event, response.data.msg);
                }
            });

        return deferred.promise;
    }

    function removeSection(data) {

        var deferred = $q.defer();

        $http({
                url: '/admin/remove_section',
                method: "POST",
                data: data
            })
            .then(function(response) {
                deferred.resolve(response.data.data);
                // $scope.component.trips.splice($index, 1);
            });

        return deferred.promise;

    }

    return fac;
}