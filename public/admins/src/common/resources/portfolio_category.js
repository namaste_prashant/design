/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.portfolio_category', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.portfolio_category').factory('PortfolioCategory', PortfolioCategory);
angular.module('resources.portfolio_category').factory('PortfolioCategoryFactory', PortfolioCategoryFactory);


function PortfolioCategory(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/portfolio_category/:id');
}


function PortfolioCategoryFactory(PortfolioCategory, BaseModelFactory, $q, $http) {
    var fac = {},
        res = PortfolioCategory;
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.list = list;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {        
        return BaseModelFactory.getDataList(res, param);
    }


    function save(data) {        
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    function list() {
        var deferred = $q.defer();
        $http.get("/admin/portfolio_category_list")
            .then(function(response) {
                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }


    return fac;
}