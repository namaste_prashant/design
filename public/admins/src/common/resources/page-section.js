/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.pageSection', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.pageSection').factory('PageSection', PageSection);
angular.module('resources.pageSection').factory('PageSectionFactory', PageSectionFactory);


function PageSection(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/page_section/:id');
}


function PageSectionFactory(PageSection, BaseModelFactory, $q, $http) {
    var fac = {},
        res = PageSection;
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.updatePageSections = updatePageSections;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(res, param);
    }


    function save(data) {
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    function updatePageSections(data) {

        var deferred = $q.defer();
        $http({
                url: '/admin/update_page_sections',
                method: "POST",
                data: data
            })
            .then(function(response) {
                if (response.data.success) {
                    deferred.resolve(response.data.success);
                } else {
                    console.log('error occoured..!!!')
                }
            });

        return deferred.promise;
    }
    return fac;
}