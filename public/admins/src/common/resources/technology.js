/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.technology', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.technology').factory('Technology', Technology);
angular.module('resources.technology').factory('TechnologyFactory', TechnologyFactory);


function Technology(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/technology/:id');
}


function TechnologyFactory(Technology, BaseModelFactory, $q, $http) {
    var fac = {},
        res = Technology;
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.list = list;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {        
        return BaseModelFactory.getDataList(res, param);
    }


    function save(data) {        
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    function list() {
        var deferred = $q.defer();
        $http.get("/admin/technology_list")
            .then(function(response) {
                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }

    return fac;
}