/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.post', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.post').factory('Post', Post);
angular.module('resources.post').factory('PostFactory', PostFactory);


function Post(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/posts/:id');
}


function PostFactory(Post, BaseModelFactory, $q, $http) {
    var fac = {};
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(Post, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(Post, param);
    }


    function save(data) {
        return BaseModelFactory.save(Post, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(Post, id);
    }

    return fac;
}