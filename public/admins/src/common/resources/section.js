/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.section', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.section').factory('Section', Section);
angular.module('resources.section').factory('SectionFactory', SectionFactory);


function Section(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/section/:id');
}


function SectionFactory(Section, BaseModelFactory, $q, $http) {
    var fac = {},
        res = Section;
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;
    fac.list = list;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(res, id);
    }


    function getDataList(param) {        
        return BaseModelFactory.getDataList(Section, param);
    }


    function save(data) {        
        return BaseModelFactory.save(res, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(res, id);
    }

    function list() {
        var deferred = $q.defer();
        $http.get("/admin/section_list")
            .then(function(response) {
                deferred.resolve(response.data.data);
            });

        return deferred.promise;
    }


    return fac;
}