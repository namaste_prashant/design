'use strict';

angular.module('app').directive('adminsidebar', [function () {
	return {
		templateUrl:'/admins/src/common/directives/sidebar/sidebar.html',
		restrict: 'E',
		replace:true,			
	};
}])
