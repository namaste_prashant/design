'use strict';

angular.module('app').directive('adminheader', [function() {
    return {
        templateUrl: '/admins/src/common/directives/header/header.html',
        restrict: 'E',
        replace: true,
        controller: function($scope, $state) {
        	
            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams) { 
                    $scope.page_title=toState.name.split('.')[0].capitalizeFirstLetter();
                });

        }
    };
}])