/**
 *  Resourse for creates read update delete.
 */
angular.module('resources.portfolio', ['ngResource', 'ngMaterial', 'rjServices']);

angular.module('resources.portfolio').factory('Portfolio', Portfolio);
angular.module('resources.portfolio').factory('PortfolioFactory', PortfolioFactory);


function Portfolio(ResourseFactory) {
    return ResourseFactory.makeResource('/admin/portfolio/:id');
}


function PortfolioFactory(Portfolio, BaseModelFactory, $q, $http) {
    var fac = {};
    fac.getDataList = getDataList;
    fac.getDataItem = getDataItem;
    fac.save = save;
    fac.remove = remove;

    function getDataItem(id) {
        return BaseModelFactory.getDataItem(Portfolio, id);
    }


    function getDataList(param) {
        return BaseModelFactory.getDataList(Portfolio, param);
    }


    function save(data) {        
        return BaseModelFactory.save(Portfolio, data);
    }


    function remove(id) {
        return BaseModelFactory.remove(Portfolio, id);
    }

    return fac;
}