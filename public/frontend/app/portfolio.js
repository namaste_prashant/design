/**
 * Shows portfolios in tab panel based on category.
 */
(function() {
    'use strict';

    angular.module('portfolio', []);
    angular.module('portfolio').controller('PortfolioController', PortfolioController);

    function PortfolioController($scope, $log, $http) {
        $scope.selectedIndex = 2;
        $scope.dataLoaded=false;
        $scope.categories = {};
        init();

        // Get initial data to run the app.
        function init(){            
            $http.get("/portfolio_category_list").then(function(response) {
                $scope.categories = response.data.data;
            });

            $http.get("/portfolio").then(function(response) {
                $scope.portfolios = response.data.data;
                $scope.dataLoaded=true;
            });            
        }    
    }
})();