(function() {
    'use strict';


    angular.module('contactus', []);
    angular.module('contactus').controller('ContactusController', ContactusController);

    function ContactusController($scope, $log, $http , $mdToast) {
        $scope.dataSaved = true;
        $scope.sendMessage = sendMessage;

        function sendMessage(dataModel ,form){            
            $scope.form=form;
            $scope.dataSaved = false;
            var req = {
                method: 'POST',
                url: '/send_message',
                data: dataModel
            }

            $http(req).then(function(response){
                if(response.data.sucess){
                    $mdToast.show(
                        $mdToast.simple()
                        .textContent("Message succesfully send")
                        .position('top right')
                        .hideDelay(2500)                       
                    );
                    $scope.dataSaved = true;                    
                    $scope.dataModel={};
                    $scope.Form.$setUntouched();                                                       
                }
            }, function(error){
                console.log('error');
            });
        }
    }

})();