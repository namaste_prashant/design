/**
 * shows images in brand stroes in rjslider.
 */
(function() {
    'use strict';

    angular.module('brand', ['slide', 'isotobes']);
    angular.module('brand').controller('BrandController', BrandController);

    function BrandController($scope, $log, $http, ModalFactory, $q) {
        $scope.showItem = showItem;
        $scope.dataList = [];
        $scope.imageList = imageList;
        init();

        function init() {
            $scope.imageList().then(function(response) {
                $scope.imageList = response;
            });
        }

        function imageList() {
            var deferred = $q.defer();
            $http.get("/gallery/brand_stories")
                .then(function(response) {
                    deferred.resolve(response.data.data);
                });
            return deferred.promise;
        }


        // Click event in image shows modal box.
        function showItem($event, $index) {            
            $event.preventDefault();
            var templateUrl = '/frontend/app/image-gallery/show-item.tpl.html',
                contrl = ShowItemController,
                data = {
                    dataList: $scope.imageList,
                    index: $index
                };
            ModalFactory.showModal($event, contrl, templateUrl, data).then(function() {});
        }
    }

    function ShowItemController($scope, data) {
        $scope.dataList = data.dataList;
        $scope.index = data.index;
    }

})();
