/**
 *  Root module for frontend angular app.
 */
angular.module('app', [
    'ngMaterial',
    'portfolio',
    'rjDirective',
    'rjServices',
    'contactus',
    'brand'
]);

/**
 *  Calling order of angularjs app.
 *  1-> app.config()
 *  2-> app.run()
 *  3-> directive's compile functions (if found in the dom)
 *  4-> app.controller()
 *  5-> directive's link functions (if found)
 *  
 */

angular.module('app').run(['$rootScope',
    function($rootScope) {
        
        // Add function to capitalizeFirstLetter to every string object.
        String.prototype.capitalizeFirstLetter = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
    }

]);


// Register controller.
angular.module('app').controller('BaseController', BaseController);

// Base Controller for in root scope.
function BaseController($scope) {
    
}

// Define route in config .
angular.module('app').config(['$interpolateProvider',
    function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    }
]);