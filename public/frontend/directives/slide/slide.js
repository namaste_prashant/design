/**
 *  reviews commponent
 *  - List the reviews of users.
 *  - Enable the user to write reviews.
 *  - How to use
 *
 *    Include the directive in template with reviewable-id and reviewable-type. e.g.
 *
 *    <reviews reviewable-id="course.id" reviewable-type="'course'"></reviews>
 *
 */

angular.module("slide", [])
    .factory("slide", function() {

        return {

        }
    })
    .directive("rjSlide", function() {

        return {

            restrict: "E",
            templateUrl: "/frontend/directives/slide/slide.html",
            replace: true,
            scope: {
                list: '=',
                index: '='
            },

            controller: function($scope) {

                $scope.previous = function($event) {

                    if ($scope.index == 0) {
                        $scope.index = $scope.list.length - 1;
                    } else {
                        $scope.index = $scope.index - 1;
                    }
                }

                $scope.next = function($event) {
                    if ($scope.list.length == ($scope.index + 1)) {
                        $scope.index = 0;
                    } else {
                        $scope.index = $scope.index + 1;
                    }
                }
            }
        };
    });