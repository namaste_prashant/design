/**
 *  isotobes commponent
 *  - list images in responsive manner .
 *  - used the parent scope.
 *  eg.
 *  <isotobes></isotobes>
 *  
 */

angular.module("isotobes", [])
    .factory("isotobes", function() {

        return {

        }
    })
    .directive("isotobes", function() {

        return {
            restrict: "AE",
            template: "<section id='photos'></section>",
            replace: true,
            link: function(scope, iElement, iAttrs) {},

            controller: function($scope, $compile) {
                $scope.imageList().then(function(response, iElement) {
                    for (var i = 0; i < response.length; i++) {
                        var width = $scope.getRandomSize(200, 400);
                        var height = $scope.getRandomSize(200, 400);

                        var img = '<img ng-click="showItem($event,' + i + ')" src="' + response[i].thumb + '" alt="[[name]]" >'
                        var temp = $compile(img)($scope);
                        angular.element(document.getElementById('photos')).append(temp);
                    }
                });

                $scope.getRandomSize = function(min, max) {
                    return Math.round(Math.random() * (max - min) + min);
                }

            }
        };
    });
