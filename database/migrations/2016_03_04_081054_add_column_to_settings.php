<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('city1');
            $table->string('city2');
            $table->string('address1');
            $table->string('address2');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('map1');
            $table->string('map2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn(['city1', 'city2', 'address1', 'address2', 'phone1', 'phone2', 'map1', 'map2']);
        });
    }
}
