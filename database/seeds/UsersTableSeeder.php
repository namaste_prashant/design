<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Janak',
            'email'    => 'janakrajbhatta@gmail.com',
            'password' => bcrypt('janak@123'),
        ]);
        DB::table('users')->insert([
            'name'     => 'admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('admin@123'),
        ]);
    }
}
